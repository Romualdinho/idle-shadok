const MAX_REFRESH_RATE = 50;

/* Classe mère */
function Producer(id, name = "Producteur", product = "?", productID = "money", price = 0, priceMultiplier = 2, productionSpeed = 5, productionMultiplier = 1, action, svg) {
	var id = id;
	var name = name;
	var product = product;
	var productID = productID;
	var count = 1;
	var progress = 0;
	var price = price;
	var priceMultiplier = priceMultiplier;
	var productionSpeed = productionSpeed;
	var productionMultiplier = productionMultiplier;
	var action = action;
	var svg = svg;

	var locked = true;

	this.unlock = function() {
		if (!locked) {
			return;
		}

		var producer = game.getProducers().get(id);

		setInterval(function() {
			if (progress >= 100) {
				progress = (100/((productionSpeed * 1000)/MAX_REFRESH_RATE));
				producer.onProgressComplete();
			} else {
				progress += (100/((productionSpeed * 1000)/MAX_REFRESH_RATE));
			}

			view.refreshProgressBar(producer);
		}, MAX_REFRESH_RATE);

		locked = false;

		view.showProducer(producer);

		if (productID === "money") {
			view.refreshProductionPerSecond();
		}
	}

	this.buy = function() {
		// Vérifie si le joueur dispose de la somme nécessaire pour acheter un producteur
		if (game.getBalance() < price) {
			view.playSound("error.wav");
			return;
		}

		game.decreaseBalance(price);
		view.playSound("buy.mp3");

		count++;

		price *= priceMultiplier;

		console.log(this);

		view.refreshProducer(game.getProducers().get(id));

		if (productID === "money") {
			view.refreshProductionPerSecond();
		}

		game.save();
	}

	this.onProgressComplete = function() {
		if (action == null) {
			throw "Action non définie";
		}

		action();
	}

	this.increaseCount = function(value) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		count += value;

		view.refreshProducer(this);

		if (productID === "money") {
			view.refreshProductionPerSecond();
		}
	}

	this.update = function(serializedProducer) {
		count = serializedProducer.count;
		price = serializedProducer.price;
		priceMultiplier = serializedProducer.priceMultiplier;
		productionSpeed = serializedProducer.productionSpeed;
		productionMultiplier = serializedProducer.productionMultiplier;
	}

	this.getId = function() {
		return id;
	}

	this.getName = function() {
		return name;
	}

	this.getProduct = function() {
		return product;
	}

	this.getProductID = function() {
		return productID;
	}

	this.getCount = function() {
		return count;
	}

	this.getProduction = function() {
		return count * productionMultiplier;
	}

	this.getProgress = function() {
		return progress;
	}

	this.getPrice = function() {
		return price;
	}

	this.getPriceMultiplier = function() {
		return priceMultiplier;
	}

	this.getProductionSpeed = function() {
		return productionSpeed;
	}

	this.getProductionMultiplier = function() {
		return productionMultiplier;
	}

	this.getSVG = function() {
		return svg;
	}

	this.isLocked = function() {
		return locked;
	}

	this.setProductionSpeed = function(value) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		productionSpeed = value;

		if (productID === "money") {
			view.refreshProductionPerSecond();
		}
	}

	this.setProductionMultiplier = function(value) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		productionMultiplier = value;

		view.refreshProducer(this);

		if (productID === "money") {
			view.refreshProductionPerSecond();
		}
	}
}

// Serialization
function SerializableProducer(producer) {
	this.id = producer.getId();
	this.name = producer.getName();
	this.count = producer.getCount();
	this.price = producer.getPrice();
	this.productID = producer.getProductID();
	this.priceMultiplier = producer.getPriceMultiplier();
	this.productionSpeed = producer.getProductionSpeed();
	this.productionMultiplier = producer.getProductionMultiplier();
	this.locked = producer.isLocked();
}
