game = new Game();
view = new View();

function init() {
	// Initialisation de la vue
	view.init();

	// Chargement du modèle
	game.load();

	// Gestion des événéments
	handleEvents();
}

function handleEvents() {
	// Statistiques
	document.getElementById("btn-stat").addEventListener("click", function() {
		view.showPopup("pop-stat");
	});
	document.getElementById("btn-stat-ok").addEventListener("click", function() {
		view.hidePopup("pop-stat")
	});

	// Sons
	document.getElementById("btn-sound").addEventListener("click", view.toggleSound);

	// Réinitialisation
	document.getElementById("btn-reset").addEventListener("click", function() {
		view.showPopup("pop-reset");
	});
	document.getElementById("btn-reset-cancel").addEventListener("click", function() {
		view.hidePopup("pop-reset");
	});
	document.getElementById("btn-reset-ok").addEventListener("click", game.reset);

	// Déconnexion
	document.getElementById("btn-disconnect").addEventListener("click", function() {
		view.showPopup("pop-disconnect");
	});
	document.getElementById("btn-disconnect-cancel").addEventListener("click", function() {
		view.hidePopup("pop-disconnect");
	});
	document.getElementById("btn-disconnect-ok").addEventListener("click", function() {
		window.location = "./php/logout.php";
	});

	// Pompage
	document.getElementById("btn_pump").addEventListener("click", function() {
		game.increaseBalance(game.currentProduction);
		view.pump();
	});

	// Améliorations
	document.getElementById("check_upgrades").addEventListener("change", view.toggleUpgradesView);

	// Producteurs
	game.getProducers().forEach(function(producer, id) {
		var table = document.getElementById(id);
		table.getElementsByClassName("producer-buy")[0].addEventListener("click", producer.buy);
	});

	game.getUpgrades().forEach(function(upgrade, id) {
		var table = document.getElementById(id);

		table.addEventListener("click", function tableHandler() {
			if (upgrade instanceof ProducerUpgrade && !upgrade.isLocked()) {
				return;
			}

			if (game.getBalance() >= upgrade.getPrice()) {
				game.getUpgrades().forEach(function(upgrade2, id) {
					var cancelButton = document.getElementById("btn_" + id + "_cancel");

					if (cancelButton != null && upgrade != upgrade2) {
						cancelButton.click();
					}
				});

				view.selectUpgrade(upgrade);

				table.removeEventListener("click", tableHandler);

				var buyButton = document.getElementById("btn_" + id + "_buy");
				buyButton.addEventListener("click", function buyHandler() {
					buyButton.removeEventListener("click", buyHandler);
					view.unselectUpgrade(upgrade);
					document.getElementById(id).addEventListener("click", tableHandler, true);

					upgrade.buy();
				});

				var cancelButton = document.getElementById("btn_" + id + "_cancel");
				cancelButton.addEventListener("click", function cancelHandler() {
					cancelButton.removeEventListener("click", cancelHandler);
					view.unselectUpgrade(upgrade);
					document.getElementById(id).addEventListener("click", tableHandler, true);
				});
			}
		});
	});
}
