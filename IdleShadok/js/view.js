function View() {
	// Teste si le naviguateur correspond à IE ou Edge
	var isIE = false || !!document.documentMode;
	var isEdge = !isIE && !!window.StyleMedia;

	var shadokSVG;
	var sound = true;

	var backgroundAmbient = new Audio('sound/nature_ambient.mp3');

	this.init = function() {
		this.installSVG();
		this.installProducers();
		this.installUpgrades();
		this.refreshAll();

		backgroundAmbient.loop = true;
		backgroundAmbient.play();
	}

	this.toggleSound = function() {
		sound = !sound;
		backgroundAmbient.volume = (sound ? 1 : 0);

		document.getElementById("btn-sound").getElementsByTagName("img")[0].setAttribute("src", sound ? "img/logos/volume_on.png" : "img/logos/volume_mute.png");
	}

	this.showPopup = function(popup) {
		document.getElementById(popup).style.display = "block";
	}

	this.hidePopup = function(popup) {
		document.getElementById(popup).style.display = "none";
	}

	this.installSVG = function() {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', "./img/shadok.svg", true);
		xhr.onreadystatechange = function (evt) {
			if (xhr.readyState === 4) {
				var shadok = document.importNode(xhr.responseXML.documentElement, true);
				document.getElementById("obj-game").contentDocument.getElementById("svg-shadok").appendChild(shadok);
			}
		};
		xhr.send(null);
	}

	this.installProducers = function() {
		game.getProducers().forEach(function(producer, id) {
			var table = document.getElementById("template_producer").cloneNode(true);

			table.id = id;
			table.getElementsByClassName("producer-name")[0].textContent = producer.getName() + " : " + producer.getCount().format();
			table.getElementsByClassName("producer-production")[0].textContent = "+" + producer.getProduction().format() + " " + producer.getProduct();
			table.getElementsByClassName("progress")[0].id = id + "_progress_bar";
			table.getElementsByClassName("producer-price")[0].textContent = producer.getPrice().format();

			document.getElementById("producers").appendChild(table);
		});
	}

	this.installUpgrades = function() {
		game.getUpgrades().forEach(function(upgrade, id) {
			var table = document.getElementById("template_upgrade_unselected").cloneNode(true);

			table.id = id;

			if (upgrade instanceof PropertyUpgrade && upgrade.getLevel() > 1) {
				table.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName() + " " + upgrade.getLevel().romanize();
			} else {
				table.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName();
			}

			table.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName();
			table.getElementsByClassName("upgrade-price")[0].textContent = upgrade.getPrice().format();
			table.getElementsByClassName("upgrade-description")[0].textContent = upgrade.getDescription();

			document.getElementById("check_upgrades_available").appendChild(table);
		});
	}

	this.toggleUpgradesView = function() {
		view.playSound("switch.wav");

		if (document.getElementById("check_upgrades").checked) {
			document.getElementById("check_upgrades_available").style.display = "none";
			document.getElementById("check_upgrades_owned").style.display = "block";
		} else {
			document.getElementById("check_upgrades_owned").style.display = "none";
			document.getElementById("check_upgrades_available").style.display = "block";
		}
	}

	this.selectUpgrade = function(upgrade) {
		var unselectedTable = document.getElementById(upgrade.getId());
		var selectedTable = document.getElementById("template_upgrade_selected").cloneNode(true);

		selectedTable.id = upgrade.getId();

		if (upgrade instanceof PropertyUpgrade && upgrade.getLevel() > 1) {
			selectedTable.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName() + " " + upgrade.getLevel().romanize();
		} else {
			selectedTable.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName();
		}

		selectedTable.getElementsByClassName("upgrade-buy")[0].id = "btn_" + upgrade.getId() + "_buy";
		selectedTable.getElementsByClassName("upgrade-cancel")[0].id = "btn_" + upgrade.getId() + "_cancel";

		unselectedTable.parentNode.replaceChild(selectedTable, unselectedTable);
	}

	this.unselectUpgrade = function(upgrade) {
		var selectedTable = document.getElementById(upgrade.getId());
		var unselectedTable = document.getElementById("template_upgrade_unselected").cloneNode(true);

		unselectedTable.id = upgrade.getId();

		if (upgrade instanceof PropertyUpgrade && upgrade.getLevel() > 1) {
			unselectedTable.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName() + " " + upgrade.getLevel().romanize();
		} else {
			unselectedTable.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName();
		}

		unselectedTable.getElementsByClassName("upgrade-price")[0].textContent = upgrade.getPrice().format();
		unselectedTable.getElementsByClassName("upgrade-description")[0].textContent = upgrade.getDescription();

		selectedTable.parentNode.replaceChild(unselectedTable, selectedTable);
	}

	this.playSound = function(file) {
		if (sound) {
			new Audio("sound/" + file).play();
		}
	}

	this.pump = function() {
		// Animations du shadok (incompatible avec Edge)
		if (!isIE && !isEdge) {
			var shadok = document.getElementById("obj-game").contentDocument.getElementById("svg-shadok");

			Array.prototype.forEach.call(shadok.getElementsByTagName("animate"), function(anim) {
				anim.beginElement();
			});

			Array.prototype.forEach.call(shadok.getElementsByTagName("animateTransform"), function(anim) {
				anim.beginElement();
			});
		}

		// Animations du texte au dessus du bouton
		var element = document.getElementById("template_click").cloneNode(true);

		element.innerText = "+" + game.getProduction().format() + " ¢";
		element.style.display = "inline";
		element.removeAttribute("id");

		var rect = document.getElementById("btn_pump").getBoundingClientRect();
		var xPosition = rect.left + Math.random() * (rect.width - 15);
		var yPosition = rect.top + Math.random() * (rect.height - 15);

		element.style.left = xPosition + "px";
		element.style.top = yPosition + "px";

		document.getElementById("my_pump").appendChild(element);

		element.classList.add("fadeOut");

		setTimeout(function() {
			document.getElementById("my_pump").removeChild(element);
		}, 900);

		// Son de la pompe
		this.playSound("pump.wav");
	}

	this.refreshBalance = function() {
		var formattedBalance = game.getBalance().format();

		document.getElementById("label_balance").textContent = formattedBalance;
		document.head.getElementsByTagName("title")[0].textContent = "Idle Shadok - " + formattedBalance + " ¢";

		setTimeout(function() {
			game.getProducers().forEach(function(producer, id) {
				var table = document.getElementById(id);

				if (game.getBalance() >= producer.getPrice()) {
					table.getElementsByClassName("producer-buy")[0].classList.add("btn-active");
					table.classList.remove("tbl-lock");
				} else {
					table.getElementsByClassName("producer-buy")[0].classList.remove("btn-active");
					table.classList.add("tbl-lock");
				}
			});
		}, 0);

		setTimeout(function() {
			game.getUpgrades().forEach(function(upgrade, id) {
				var table = document.getElementById(id);

				if (game.getBalance() >= upgrade.getPrice() || (upgrade instanceof ProducerUpgrade && !upgrade.isLocked())) {
					table.classList.remove("tbl-lock");
				} else {
					table.classList.add("tbl-lock");
				}
			});
		}, 0);
	}

	this.refreshProductionPerSecond = function() {
		setTimeout(function() {
			var value = 0;
			game.getProducers().forEach(function(producer, id) {
				if (producer.getProductID() === "money" && !producer.isLocked()) {
					value += producer.getProduction() / producer.getProductionSpeed();
				}
			});

			document.getElementById("lbl_production").textContent = value.format();
		}, 0);
	}

	this.refreshProduction = function() {
		document.getElementById("lbl_production").textContent = game.getProduction().format();
	}

	this.showProducer = function(producer) {
		document.getElementById("producers").style.display = "block";
		document.getElementById(producer.getId()).style.display = "inline-table";

		var pathSVG = producer.getSVG();
		var background = document.getElementById("obj-game").contentDocument;
		var signs = background.getElementsByClassName("sign-sale");

		if (pathSVG != null && signs.length > 0) {
			var xhr = new XMLHttpRequest();
			xhr.open('GET', pathSVG, true);
			xhr.onreadystatechange = function (evt) {
				if (xhr.readyState === 4) {
					var selectedSign = signs[0];
					var parentNode = selectedSign.parentNode;

					var svg = background.getElementById("template_producer").cloneNode(true);
					svg.setAttribute("x", selectedSign.getAttribute("data-x"));
					svg.setAttribute("y", selectedSign.getAttribute("data-y"));
					svg.setAttribute("width", selectedSign.getAttribute("data-scale"));
					svg.setAttribute("height", selectedSign.getAttribute("data-scale"));
					svg.appendChild(document.importNode(xhr.responseXML.documentElement, true));

					parentNode.appendChild(svg);
					parentNode.removeChild(selectedSign);
				}
			};
			xhr.send(null);
		}
	}

	this.refreshProducer = function(producer) {
		var table = document.getElementById(producer.getId());

		table.getElementsByClassName("producer-name")[0].textContent = producer.getName() + " : " + producer.getCount().format();
		table.getElementsByClassName("producer-production")[0].textContent = "+" + producer.getProduction().format() + " " + producer.getProduct();
		table.getElementsByClassName("producer-price")[0].textContent = producer.getPrice().format();
	}

	this.refreshProducers = function() {
		game.getProducers().forEach(function(producer, id) {
			view.refreshProducer(producer);
		});
	}

	this.refreshProgressBar = function(producer) {
		document.getElementById(producer.getId()).getElementsByClassName("progress-bar")[0].style.width = producer.getProgress() + "%";
	}

	this.refreshUpgrade = function(upgrade) {
		var table = document.getElementById(upgrade.getId());

		if (upgrade instanceof PropertyUpgrade && upgrade.getLevel() > 1) {
			table.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName() + " " + upgrade.getLevel().romanize();
		} else {
			table.getElementsByClassName("upgrade-title")[0].textContent = upgrade.getName();
		}

		table.getElementsByClassName("upgrade-price")[0].textContent = upgrade.getPrice().format();
		table.getElementsByClassName("upgrade-description")[0].textContent = upgrade.getDescription();
	}

	this.refreshUpgrades = function() {
		game.getUpgrades().forEach(function(upgrade, id) {
			var table = document.getElementById(id);
			table.style.display = (upgrade.isAvailable() ? "inline-table" : "none");

			if (upgrade instanceof ProducerUpgrade) {
				document.getElementById(upgrade.isLocked() ? "check_upgrades_available" : "check_upgrades_owned").appendChild(table);
			}

			view.refreshUpgrade(upgrade);
		});
	}

	this.refreshStat = function(data) {
		var old = JSON.parse(data.oldSave);
		var text = '<tr><td>Balance</td><td class="txt-right">' + old.balance.format() + '</td><td class="txt-left txt-green">(+ ' + (data.balance - old.balance).format() + ')</td></tr>';

		for (var i = 0; i < data.producers.length; i++) {
			if (!data.producers[i].locked) {
				if (data.producers[i].count != old.producers[i].count) {
					text += '<tr><td>' + data.producers[i].name + '</td><td class="txt-right">' + old.producers[i].count.format() + '</td><td class="txt-left txt-green">(+ ' + (data.producers[i].count - old.producers[i].count).format() + ')</td></tr>';
				} else {
					text += '<tr><td>' + data.producers[i].name + '</td><td class="txt-right">' + data.producers[i].count.format() + '</td><td class="txt-left txt-neutral">(=)</td></tr>';
				}
			}
		}

		document.getElementById("tbl-stat").innerHTML = text;
	}

	this.refreshAll = function() {
		this.refreshBalance();
		this.refreshProductionPerSecond();
		this.refreshProducers();
		this.refreshUpgrades();
	}
}
