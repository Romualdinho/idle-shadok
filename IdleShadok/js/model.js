function Game() {
	var balance = 0;
	var production = 1;

	var upgrades = new Map(); {
		// Améliorations ajoutant des producteurs
		upgrades.set("upgrade_enrollment", new ProducerUpgrade("upgrade_enrollment", "Recruter", "Capacité de recruter des pompeurs", 20, function() {
			producers.get("producer_pumper").unlock();
		}));

		upgrades.set("upgrade_blue_print", new ProducerUpgrade("upgrade_blue_print", "Plan de construction", "Capacité de recruter des constructeurs", 200, function() {
			producers.get("producer_builder").unlock();
		}, ["upgrade_enrollment"]));

		upgrades.set("upgrade_knowledge_transfer", new ProducerUpgrade("upgrade_knowledge_transfer", "Transfert connaissances", "Capacité de construire des universités", 2000, function() {
			producers.get("producer_university").unlock();
		}, ["upgrade_blue_print"]));

		upgrades.set("upgrade_shadok_bank", new ProducerUpgrade("upgrade_shadok_bank", "Shadok Bank", "Capacité de construire des banques", 1000, function() {
			producers.get("producer_shadok_bank").unlock();
		}, ["upgrade_knowledge_transfer"]));


		// Améliorations de propriété
		upgrades.set("upgrade_pump", new PropertyUpgrade("upgrade_pump", "Super pompe", "Augmentation de la production de la pompe de 50%", 100, 2, function() {
			game.setProduction(game.getProduction() * 1.5);
		}));

		upgrades.set("upgrade_pump_efficiency", new PropertyUpgrade("upgrade_pump_efficiency", "Efficacité pompes", "Augmentation de la production des pompeurs de 2%", 30, 1.8, function() {
			var producer = producers.get("producer_pumper");
			producer.setProductionMultiplier(producer.getProductionMultiplier() * 1.02);
		}, ["upgrade_enrollment"]));

		upgrades.set("upgrade_steroides", new PropertyUpgrade("upgrade_steroides", "Steroïdes", "Augmentation de la vitesse de pompage de 2%", 30, 1.8, function() {
			var producer = producers.get("producer_pumper");
			producer.setProductionSpeed(producer.getProductionSpeed() * 0.98);
		}, ["upgrade_enrollment"]));

		upgrades.set("upgrade_interests", new PropertyUpgrade("upgrade_interests", "Intérêts", "Augmentation des intérêts des Shadok Bank de 2%", 50, 1.8, function() {
			var producer = producers.get("producer_shadok_bank");
			producer.setProductionMultiplier(producer.getProductionMultiplier() * 1.02);
		}, ["upgrade_shadok_bank"]));
	}

	var producers = new Map(); {
		producers.set("producer_pumper", new Producer("producer_pumper", "Pompeurs", "¢", "money", 20, 1.2, 5, 1, function() {
			game.increaseBalance(producers.get("producer_pumper").getProduction());
		}, "./img/center.svg"));

		producers.set("producer_builder", new Producer("producer_builder", "Constructeurs", "pompeur(s)", "producer_pumper", 160, 1.2, 5, 1, function() {
			var producer = producers.get("producer_pumper");
			producer.increaseCount(producers.get("producer_builder").getProduction());
			view.refreshProducer(producer);
		}, "./img/factory.svg"));

		producers.set("producer_university", new Producer("producer_university", "Université", "constructeur(s)", "producer_builder", 800, 1.2, 5, 1, function() {
			var producer = producers.get("producer_builder");
			producer.increaseCount(producers.get("producer_university").getProduction());
			view.refreshProducer(producer);
		}, "./img/university.svg"));

		producers.set("producer_shadok_bank", new Producer("producer_shadok_bank", "Shadock Bank", "¢", "money", 1200, 2, 25, 1000, function() {
			game.increaseBalance(producers.get("producer_shadok_bank").getProduction());
		}, "./img/bank.svg"));
	}


	this.increaseBalance = function(value = production) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		balance += value;
		view.refreshBalance();

		this.save();
	}

	this.decreaseBalance = function(value = 1) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		if (balance - value < 0) {
			throw "La balance ne peut pas être négative";
		}

		balance -= value;
		view.refreshBalance();

		this.save();
	}

	this.setProduction = function(value) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}

		if (value <= 0) {
			throw "La valeur doit être supérieure à 1";
		}

		production = value;
	}

	this.save = function() {
		var data = JSON.stringify(new SerializableGame(this));

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "./php/save.php", true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.send("data=" + data);
	}

	this.load = function() {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "./php/load.php", true);

		xhr.onreadystatechange = function (evt) {
			if (xhr.readyState === 4) {
				if (xhr.responseText.length == 0) {
					return;
				}

				var data = JSON.parse(xhr.responseText);

				if (data == null) {
					return;
				}

				balance = data.balance;
				production = data.production;

				for (var i = 0; i < data.upgrades.length; i++) {
					var savedUpgrade = data.upgrades[i];
					var upgrade = upgrades.get(savedUpgrade.id);
					upgrade.update(savedUpgrade);
				}

				for (var i = 0; i < data.producers.length; i++) {
					var savedProducer = data.producers[i];
					var producer = producers.get(savedProducer.id);
					producer.update(savedProducer);

					if (!savedProducer.locked) {
						producer.unlock();
					}
				}

				view.refreshStat(data);
				view.refreshAll();
			}
		};
		xhr.send(null);
	}

	this.reset = function() {
		window.location = "./php/reset.php";
	}

	this.getPlayerName = function() {
		return playerName;
	}

	this.getBalance = function() {
		return balance;
	}

	this.getProduction = function() {
		return production;
	}

	this.getUpgrades = function() {
		return upgrades;
	}

	this.getProducers = function() {
		return producers;
	}
}

function SerializableGame(game) {
	this.balance = game.getBalance();
	this.production = game.getProduction();

	var upgrades = new Array();
	game.getUpgrades().forEach(function(upgrade, id) {
		upgrades.push(new SerializableUpgrade(upgrade));
	});
	this.upgrades = upgrades;

	var producers = new Array();
	game.getProducers().forEach(function(producer, id) {
		producers.push(new SerializableProducer(producer));
	});
	this.producers = producers;
}

Number.prototype.toFixedFloor = function(decimals) {
	return this.toString().match(new RegExp('^-?\\d+(?:\.\\d{0,' + (decimals || -1) + '})?'))[0];
}

Number.prototype.format = function() {
	var value = this.valueOf();

	if (value >= 1000) {
		var abbreviations = ["K", "M", "B", "T", "KT", "MT", "BT", "TT", "KTT", "MTT", "BTT", "TTT", "KTTT", "MTTT", "BTTT", "TTTT"];

		for (var i = 1; i < abbreviations.length; i++) {
			var abbrValue = Math.pow(10, (i + 1) * 3);
			if (value < abbrValue) {
				return (value/Math.pow(10, i * 3)).toFixedFloor(1) + " " + abbreviations[i - 1];
			}
		}
	}

	return value.toFixedFloor(1);
}

Number.prototype.romanize = function() {
	var value = this.valueOf();

	var lookup = {M:1000, CM:900, D:500, CD:400, C:100, XC:90, L:50, XL:40, X:10, IX:9, V:5, IV:4, I:1};
	var roman = '';

	for (var i in lookup) {
		while (value >= lookup[i]) {
			roman += i;
			value -= lookup[i];
		}
	}

	return roman;
}
