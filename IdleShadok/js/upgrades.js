// Classe mère

function AbstractUpgrade(id, name = "Amélioration", description = "Pas de description", price = 1, action, requiredUpgrades = new Array()) {
	var id = id;
	var name = name;
	var description = description;
	var price = price;
	var action = action;
	var requiredUpgrades = requiredUpgrades;
	
	this.doAction = function() {
		if (action == null) {
			throw "Action non définie";
		}
		
		action();
	}
	
	this.isAvailable = function() {
		for (var i = 0; i < requiredUpgrades.length; i++) {
			
			var upgrade = game.getUpgrades().get(requiredUpgrades[i]);
			
			if (upgrade == null) {
				throw "Une amélioration requise est inexistante";
			}
			
			if (upgrade.isLocked()) {
				return false;
			}
		}
		
		return true;
	}
	
	this.getId = function() {
		return id;
	}
	
	this.getName = function() {
		return name;
	}
	
	this.getDescription = function() {
		return description;
	}
	
	this.getPrice = function() {
		return price;
	}
	
	this.setPrice = function(value) {
		if (isNaN(value)) {
			throw "La valeur doit être un nombre";
		}
		
		if (value <= 0) {
			throw "La valeur doit être supérieure à 0";
		}
		
		price = value;
	}
}

AbstractUpgrade.prototype.buy = function() {
		// Vérifie si le joueur dispose de la somme nécessaire pour débloquer cette amélioration
		if (game.getBalance() < this.getPrice()) {
			view.playSound("error.wav");
			return;
		}

		// Vérifie si toutes les améliorations nécessaires ont été débloquées
		if (!this.isAvailable()) {
			throw "L'amélioration n'est pas disponible, des améliorations nécessaires doivent être préalablement débloquées";
		}
		
		view.playSound("upgrade.wav");
		game.decreaseBalance(this.getPrice());
		
		// Executes l'action de l'amélioration
		this.doAction();
}

// Classes filles

function ProducerUpgrade(id, name, description = "Pas de description", price = 0, action, requiredUpgrades = new Array()) {
	AbstractUpgrade.call(this, id, name, description, price, action, requiredUpgrades);
	
	var locked = true;
	
	this.buy = function() {
		AbstractUpgrade.prototype.buy.call(this);
		
		locked = false;
		
		game.save();
		
		view.refreshUpgrades();
	}
	
	this.update = function(serializedUpgrade) {
		this.setPrice(serializedUpgrade.price);
		locked = serializedUpgrade.locked;
	}
	
	this.isLocked = function() {
		return locked;
	}
}

function PropertyUpgrade(id, name, description = "Pas de description", price = 0, priceMultiplier = 1, action, requiredUpgrades = new Array()) {
	AbstractUpgrade.call(this, id, name, description, price, action, requiredUpgrades);

	var level = 1;
	var priceMultiplier = priceMultiplier;
	
	this.buy = function() {
		AbstractUpgrade.prototype.buy.call(this);
		
		this.setPrice(this.getPrice() * priceMultiplier);
		level++;
		
		game.save();
		
		view.refreshUpgrade(this);
	}
	
	this.update = function(serializedUpgrade) {
		this.setPrice(serializedUpgrade.price);
		level = serializedUpgrade.level;
		priceMultiplier = serializedUpgrade.priceMultiplier;
	}
	
	this.getLevel = function() {
		return level;
	}
	
	this.getPriceMultiplier = function() {
		return priceMultiplier;
	}
}

ProducerUpgrade.prototype = Object.create(AbstractUpgrade.prototype);
PropertyUpgrade.prototype = Object.create(AbstractUpgrade.prototype);
ProducerUpgrade.prototype.constructor = ProducerUpgrade;
PropertyUpgrade.prototype.constructor = PropertyUpgrade;

// Serialization

function SerializableUpgrade(upgrade) {
	this.id = upgrade.getId();
	this.price = upgrade.getPrice();
	
	if (upgrade instanceof ProducerUpgrade) {
		this.locked = upgrade.isLocked();
	} else if (upgrade instanceof PropertyUpgrade) {
		this.level = upgrade.getLevel();
		this.priceMultiplier = upgrade.getPriceMultiplier();
	}
}