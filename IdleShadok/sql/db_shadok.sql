-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 07 Janvier 2018 à 18:40
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_shadok`
--

-- --------------------------------------------------------

--
-- Structure de la table `tbl_save`
--

CREATE TABLE `tbl_save` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `last_connection` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `game` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tbl_save`
--

INSERT INTO `tbl_save` (`id`, `user`, `last_connection`, `game`) VALUES
(1, 1, '2018-01-07 18:37:05', '{"balance":19536.650390625,"production":38.443359375,"upgrades":[{"id":"upgrade_enrollment","price":20,"locked":true},{"id":"upgrade_blue_print","price":200,"locked":true},{"id":"upgrade_knowledge_transfer","price":2000,"locked":true},{"id":"upgrade_shadok_bank","price":1000,"locked":true},{"id":"upgrade_pump","price":51200,"level":10,"priceMultiplier":2},{"id":"upgrade_pump_efficiency","price":30,"level":1,"priceMultiplier":1.8},{"id":"upgrade_steroides","price":30,"level":1,"priceMultiplier":1.8},{"id":"upgrade_interests","price":50,"level":1,"priceMultiplier":1.8}],"producers":[{"id":"producer_pumper","name":"Pompeurs","count":1,"price":20,"productID":"money","priceMultiplier":1.2,"productionSpeed":5,"productionMultiplier":1,"locked":true},{"id":"producer_builder","name":"Constructeurs","count":1,"price":160,"productID":"producer_pumper","priceMultiplier":1.2,"productionSpeed":5,"productionMultiplier":1,"locked":true},{"id":"producer_university","name":"UniversitÃ©","count":1,"price":800,"productID":"producer_builder","priceMultiplier":1.2,"productionSpeed":5,"productionMultiplier":1,"locked":true},{"id":"producer_shadok_bank","name":"Shadock Bank","count":1,"price":1200,"productID":"money","priceMultiplier":2,"productionSpeed":25,"productionMultiplier":1000,"locked":true}]}'),
(2, 2, '2018-01-07 18:40:14', '{"balance":106.31020230881704,"production":25.62890625,"upgrades":[{"id":"upgrade_enrollment","price":20,"locked":false},{"id":"upgrade_blue_print","price":200,"locked":false},{"id":"upgrade_knowledge_transfer","price":2000,"locked":false},{"id":"upgrade_shadok_bank","price":1000,"locked":false},{"id":"upgrade_pump","price":25600,"level":9,"priceMultiplier":2},{"id":"upgrade_pump_efficiency","price":34704.94144278529,"level":13,"priceMultiplier":1.8},{"id":"upgrade_steroides","price":19280.523023769605,"level":12,"priceMultiplier":1.8},{"id":"upgrade_interests","price":32134.205039616012,"level":12,"priceMultiplier":1.8}],"producers":[{"id":"producer_pumper","name":"Pompeurs","count":59,"price":71.66361599999998,"productID":"money","priceMultiplier":1.2,"productionSpeed":4.00365675374898,"productionMultiplier":1.2682417945625455,"locked":false},{"id":"producer_builder","name":"Constructeurs","count":26,"price":825.5648563199996,"productID":"producer_pumper","priceMultiplier":1.2,"productionSpeed":5,"productionMultiplier":1,"locked":false},{"id":"producer_university","name":"UniversitÃ©","count":7,"price":2388.7871999999998,"productID":"producer_builder","priceMultiplier":1.2,"productionSpeed":5,"productionMultiplier":1,"locked":false},{"id":"producer_shadok_bank","name":"Shadock Bank","count":3,"price":4800,"productID":"money","priceMultiplier":2,"productionSpeed":25,"productionMultiplier":1243.3743083946526,"locked":false}]}');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `login`, `password`) VALUES
(1, 'toto', '$2y$10$Pem.tlemNBEQeaNWhYGGAeuk87FxWDlYIaGujzp.n93ysswgHWNrO'),
(2, 'titi', '$2y$10$ebU56ZVpuAxPm3FVAXhZJecYWCkQQulL80.2vuvtT2QGhaY.CHs.O');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `tbl_save`
--
ALTER TABLE `tbl_save`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `tbl_save`
--
ALTER TABLE `tbl_save`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
