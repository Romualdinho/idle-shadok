<?php
session_start();

if ($_SESSION['connect'] != 1) {
  header('Location: connection.php');
  exit;
}
?>

<!DOCTYPE HTML>
<html>
<head>
  <title>Idle Shadok</title>
  <meta charset="utf-8">
  <link rel="icon" href="img/icon.png">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/buttons.css">
  <link rel="stylesheet" type="text/css" href="css/screen.css">
  <script src="js/upgrades.js"></script>
  <script src="js/producers.js"></script>
  <script src="js/model.js"></script>
  <script src="js/view.js"></script>
  <script src="js/controller.js"></script>
</head>
<body onload="init()">

  <!-- Templates -->
  <span class="click" id="template_click" hidden></span>

  <table id="template_upgrade_unselected" hidden>
    <tr>
      <td class="upgrade-title" colspan="2"></td>
    </tr>
    <tr>
      <td>Prix : <span class="upgrade-price"></span>&nbsp;&cent;</td>
      <td class="btn btn-info tooltip">
        <button type="button">?</button>
        <span class="tooltiptext upgrade-description"></span>
      </td>
    </tr>
  </table>

  <table id="template_upgrade_selected" hidden>
    <tr>
      <td class="upgrade-title" colspan="2"></td>
    </tr>
    <tr>
      <td class="btn btn-active upgrade-buy">
        <button type="button">Acheter</button>
      </td>
      <td class="btn btn-hand-cursor upgrade-cancel">
        <button type="button">Annuler</button>
      </td>
    </tr>
  </table>

  <table id="template_producer" hidden>
    <tr>
      <td class="producer-name" colspan="2"></td>
    </tr>
    <tr>
      <td colspan="2">
        <div class="progress">
          <span class="producer-production"></span>
          <div class="progress-bar" id="producer_progress_bar"></div>
        </div>
      </td>
    </tr>
    <tr>
      <td>Prix : <span class="producer-price"></span>&nbsp;&cent;</td>
      <td class="btn producer-buy">
        <button type="button">Acheter</button>
      </td>
    </tr>
  </table>

  <!-- Réinitialiser -->
  <div id="pop-reset" class="popup">
    <div>
      <table>
        <th colspan="2"><h2>Réinitialiser</h2></th>
        <tr>
          <td colspan="2">
            Voulez-vous réinitialiser le jeu ?<br>
            Toute votre progression sera perdue.
          </td>
        </tr>
        <tr class="btn-group-pop">
          <td class="btn btn-warning">
            <button id="btn-reset-ok" type="button">Confirmer</button>
          </td>
          <td class="btn btn-hand-cursor">
            <button id="btn-reset-cancel" type="button">Annuler</button>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <!-- Déconnexion -->
  <div id="pop-disconnect" class="popup">
    <div>
      <table>
        <th colspan="2"><h2>Déconnexion</h2></th>
        <tr>
          <td colspan="2">
            Voulez-vous vraiment quitter le jeu ?
          </td>
        </tr>
        <tr class="btn-group-pop">
          <td class="btn btn-warning">
            <button id="btn-disconnect-ok" type="button">Quitter</button>
          </td>
          <td class="btn btn-hand-cursor">
            <button id="btn-disconnect-cancel" type="button">Annuler</button>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <!-- Statistiques -->
  <div id="pop-stat" class="popup">
    <div>
      <table>
        <thead>
          <th colspan="3"><h2>Statistiques</h2></th>
        </thead>
        <tbody id="tbl-stat">
        </tbody>
        <tfoot>
          <tr class=" btn-group-pop">
            <td class="btn btn-active" colspan="3">
              <button id="btn-stat-ok" type="button">Reprendre</button>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

<header>
  <table>
    <tr>
      <td id="hdr-home" class="hdr-text btn btn-info">
        <div class="tabs">
          <button id="btn-stat"><img src="img/logos/graphics.png" alt="Statistiques"></button>
          <button id="btn-sound"><img src="img/logos/volume_on.png" alt="Sons"></button>
          <button id="btn-reset"><img src="img/logos/reload.png" alt="Réinitialiser"></button>
          <button id="btn-disconnect"><img src="img/logos/disconnection.png" alt="Déconnexion"></button>
        </div>
      </td>
      <td id="hdr-title" class="hdr-text">Idle Shadok - <?= ucfirst($_SESSION['login']) ?></td>
      <td id="hdr-balance" class="hdr-text">
        <span id="label_balance">0</span>&nbsp;<img src="img/cent.png" alt="Cent">
      </td>
    </tr>
  </table>
</header>

<!-- Achats -->
<aside class="block_aside">
  <!-- Achats - Ma pompe -->
  <table id="my_pump">
    <tr>
      <td class="center"><button type="button" id="btn_pump" class="my_click">Pomper</button></td>
    </tr>
    <tr>
      <td class="center"><p>Production: <span id="lbl_production">0</span>&nbsp;&cent;/s</p></td>
    </tr>
  </table>

  <div class="show_border producers" id="producers" hidden>
  </div>
</aside>

<!-- Améliorations -->
<article class="updates block_article show_border">
  <!-- Changement Amélioration / Possédé -->
  <label class="switch" id="switch_upgrades">
    <input type="checkbox" id="check_upgrades">
    <span class="slider"></span>
  </label>

  <!-- Améliorations disponibles -->
  <div id="check_upgrades_available">
    <h3>Améliorations</h3>
  </div>

  <!-- Améliorations déjà possédé -->
  <div id="check_upgrades_owned" class="owned" style="display:none;">
    <h3>Déjà acquis</h3>
  </div>
</article>

<!-- Animation -->
<section class="block_section">
  <div>
    <div class="svg-container">
      <object type="image/svg+xml" data="img/back.svg" id="obj-game" class="svg-content"></object>
    </div>
  </div>
</section>

<!-- Zone de déverminage -->
<footer class="show_border center">
  <div>
  <math xmlns="http://www.w3.org/1998/Math/MathML">
    <mi>Cosmogol</mi><mo>=</mo><mi>S['Cosmogol']</mi><mo>+</mo>
      <mi>S['Pompeurs']</mi><mo>*</mo><mi>P['Pompeurs']</mi><mo>+</mo>
      <mo>(</mo><mi>S['Constructeurs']</mi><mo>*</mo><mfrac><msup><mi>P['Constructeurs']</mi><mn>2</mn></msup><mn>2</mn></mfrac><mo>)</mo><mo>+</mo>
      <mo>(</mo><mi>S['Universités']</mi><mo>*</mo><mfrac><msup><mi>P['Universités']</mi><mn>3</mn></msup><mn>6</mn></mfrac><mo>)</mo></math></footer>
</body>
</html>
