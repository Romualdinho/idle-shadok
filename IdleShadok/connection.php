<?php
  session_start();

  $_SESSION['connect'] = 0;
?>

<!DOCTYPE HTML>
<html>
<head>
  <title>Idle Shadok</title>
  <meta charset="utf-8">
  <link rel="icon" href="img/icon.png">
  <link rel="stylesheet" type="text/css" href="css/connexion.css">
  <link rel="stylesheet" type="text/css" href="css/buttons.css">
</head>
<body>
  <header>Connexion</header>
  <div class="back-white">
    <form action="php/verify_connection.php" method="POST">
        <h1>Identifiant</h1>
        <input class="txt-login" type="text" name="login" placeholder="Pseudo"/>
        <h1>Mot de passe</h1>
        <input class="txt-login" type="password" name="password" placeholder="Mot de passe"/>

        <!-- Affichage des erreurs -->
        <?php include 'php/error.inc.php' ?>

        <p class="btn btn-login"><input type="submit" value="Entrer"></p>
        <p><a href="account.php">Créer un compte ?</a></p>
    </form>
  </div>
</body>
</html>
