<?php
session_start();

include_once "db.php";

if ($conn != null) {
	$stmt = $conn->prepare("SELECT last_connection, game FROM tbl_save WHERE user = :user");
	$stmt->bindParam(':user', $_SESSION['id']);

	$stmt->execute();
	$result = $stmt->fetchAll();

	echo updateGame($result[0]['last_connection'], $result[0]['game']);
}

// Mise à jour hors jeu
function updateGame($last, $data) {
	$time = strtotime(date('Y-m-d H:i:s')) - strtotime($last);
	$json = json_decode($data, true);

	for ($i = count($json['producers']) - 1; $i >= 0; $i--) {
		if ($json['producers'][$i]['locked'] == false) {
			if ($json['producers'][$i]['productID'] === "money") {
				// Mise à jour de la balance
				$json['balance'] += (($json['producers'][$i]['count'] * $json['producers'][$i]['productionMultiplier']) / $json['producers'][$i]['productionSpeed']) * $time;
			} else {
				for ($j = 0; $j < count($json['producers']); $j++) {
					if ($json['producers'][$i]['productID'] === $json['producers'][$j]['id']) {
						// Mise à jour des producteurs
						$json['producers'][$j]['count'] += round(($json['producers'][$i]['count'] * $json['producers'][$i]['productionMultiplier']) / $json['producers'][$i]['productionSpeed']) * $time;
					}
				}
			}
		}
	}

	if (isset($data) && !empty($data)) {
		$json['oldSave'] = $data;
	}

	return json_encode($json);
}
?>
