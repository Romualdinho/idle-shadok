<?php

if (isset($_GET['e']) && !empty($_GET['e'])) {
  switch ($_GET['e']) {
    case 1:
      echo '<p class="txt-red">Taille de l\'identifiant trop importante</p>';
      break;
    case 2:
      echo '<p class="txt-red">Identifiant vide</p>';
        break;
    case 3:
      echo '<p class="txt-red">Mot de passe vide</p>';
        break;
    case 4:
      echo '<p class="txt-red">Mauvais login et/ou mot de passe</p>';
        break;
    case 5:
      echo '<p class="txt-red">Merci de vous connecter</p>';
      break;
    case 6:
      echo '<p class="txt-red">Connexion échouée à la base de données</p>';
      break;
    case 7:
      echo '<p class="txt-red">Taille minimale du mot de passe insuffisant (4)</p>';
      break;
    case 8:
      echo '<p class="txt-red">Pseudo déjà existant</p>';
      break;
    case 9:
      echo '<p class="txt-red">Mots de passe non identiques</p>';
      break;
    default:
      echo '<p class="txt-red">&nbsp;</p>';
      break;
  }
} else {
  echo '<p class="txt-red">&nbsp;</p>';
}

?>
