<?php
$servername = "localhost";
$database = "db_shadok";
$username = "root";
$password = "m1shadoks";

try {
  // Connexion à la base de données
  $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  // Erreur (Base de données)
  header('Location: ../connection.php?e=6');
  exit;
}

?>
