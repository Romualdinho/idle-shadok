<?php
  session_start();

// Inclusion des fichiers
include_once "db.php";

// Vérification des POSTs
if (isset($_POST['login']) && !empty(strip_tags($_POST['login']))) {
  // Taille du pseudo
  if (strlen(strip_tags($_POST['login'])) <= 90) {
    $login = strtolower(strip_tags($_POST['login']));
  } else {
    // Erreur (Taille pseudo)
    header('Location: ../account.php?e=1');
    exit;
  }
} else {
  // Erreur (Champ login vide)
  header('Location: ../account.php?e=2');
  exit;
}

if (isset($_POST['password']) && !empty($_POST['password']) && isset($_POST['password-confirm']) && !empty($_POST['password-confirm']) && strlen(strip_tags($_POST['password'])) >= 4) {
  if ($_POST['password'] === $_POST['password-confirm']) {
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
  } else {
    // Erreur (MDP différents)
    header('Location: ../account.php?e=9');
    exit;
  }
} else {
  // Erreur (Champ MDP vide)
  header('Location: ../account.php?e=7');
  exit;
}

// Connexion OK
if ($conn != null) {

  // Vérification existance du pseudo
  $stmt = $conn->prepare("SELECT COUNT(*) FROM tbl_users WHERE login = :user");
  $stmt->bindParam(':user', $login);

  // Données
  $stmt->execute();
  $row = $stmt->fetchColumn();

  // Vérification du mot de passe
  if ($row >= 1) {
    // Erreur (Pseudo existant)
    header('Location: ../account.php?e=8');
    exit;
  } else {
    // Préparation de la requête
    $stmt = $conn->prepare("INSERT INTO tbl_users (login, password) VALUES (:user, :pass)");
    $stmt->bindParam(':user', $login);
    $stmt->bindParam(':pass', $password);

    // Execution
    $stmt->execute();

    $_SESSION['login'] = $login;
    $_SESSION['id'] = $conn->lastInsertId();
    $_SESSION['connect'] = 1;

    // Préparation de la requête
    $stmt = $conn->prepare("INSERT INTO tbl_save (user) VALUES (:id)");
    $stmt->bindParam(':id', $_SESSION['id']);

    // Execution
    $stmt->execute();



    header('Location: ../index.php');
  }
}
?>
