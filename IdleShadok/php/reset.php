<?php
session_start();

include_once "db.php";

if ($conn != null) {
  $stmt = $conn->prepare("UPDATE tbl_save SET last_connection = :current, game = NULL WHERE user = :user");
  $stmt->bindParam(':current', date('Y-m-d H:i:s'));
  $stmt->bindParam(':user', $_SESSION['id']);
	$stmt->execute();

	header('Location: ../index.php');
}
?>
