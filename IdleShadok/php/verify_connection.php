<?php
  session_start();

// Inclusion des fichiers
include_once "db.php";

// Vérification des POSTs
if (isset($_POST['login']) && !empty(strip_tags($_POST['login']))) {
  // Taille du pseudo
  if (strlen(strip_tags($_POST['login'])) <= 90) {
    $login = strtolower(strip_tags($_POST['login']));
  } else {
    // Erreur (Taille pseudo)
    header('Location: ../connection.php?e=1');
    exit;
  }
} else {
  // Erreur (Champ login vide)
  header('Location: ../connection.php?e=2');
  exit;
}

if (isset($_POST['password']) && !empty($_POST['password'])) {
  $password = $_POST['password'];
} else {
  // Erreur (Champ MDP vide)
  header('Location: ../connection.php?e=3');
  exit;
}

// Connexion OK
if ($conn != null) {
  // Préparation de la requête
  $stmt = $conn->prepare("SELECT id, password FROM tbl_users WHERE login = :user");
  $stmt->bindParam(':user', $login);

  // Données
  $stmt->execute();
  $row = $stmt->fetch();

  // Vérification du mot de passe
  if (password_verify($password, $row['password'])) {
    // Mot de passe OK
    $_SESSION['login'] = $login;
    $_SESSION['id'] = $row['id'];
    $_SESSION['connect'] = 1;
    header('Location: ../index.php');
    exit;
  } else {
    // Erreur (Mauvais Login / Mot de passe)
    header('Location: ../connection.php?e=4');
    exit;
  }
}
?>
