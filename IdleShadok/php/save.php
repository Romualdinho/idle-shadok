<?php
session_start();

include_once "db.php";

if ($_POST["data"] && $conn != null) {
	$date = date('Y-m-d H:i:s');
	$stmt = $conn->prepare("UPDATE tbl_save SET last_connection = :current, game = :data WHERE user = :user");
	$stmt->bindParam(':current', $date);
	$stmt->bindParam(':data', $_POST["data"]);
	$stmt->bindParam(':user', $_SESSION['id']);

	$stmt->execute();
}
?>
